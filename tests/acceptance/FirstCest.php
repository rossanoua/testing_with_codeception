<?php


class FirstCest
{
    public function _before(AcceptanceTester $I)
    {
    }

    public function _after(AcceptanceTester $I)
    {
    }

    // tests
    public function tryToTest(AcceptanceTester $I)
    {
        $I->amOnPage('/');
        $I->wantTo('index page is working');
        $I->see('hello gulp');

        if (method_exists($I, 'wait')){
            $I->wait(5);
        }


    }
}
